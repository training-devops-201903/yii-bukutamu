# Cara Membuat Docker Image #

1. Install PHP 7.2 dan extension yang dibutuhkan 

    ```
     apt update && apt upgrade -y
     apt install php7.2 php7.2-gd php7.2-mbstring php7.2-dom php7.2-zip zip unzip -y
    ```

2. Install Composer

   ```
   wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8
   a2700e2/web/installer -O - -q | php -- --quiet
   mv composer.phar /usr/local/bin/composer
   ```

3. Clone project

   ```
   git clone https://gitlab.com/training-devops-201903/yii-bukutamu.git
   ```

4. Build project

   ```
   cd yii-bukutamu
   composer install
   ```
   
   Outputnya seperti ini
   
   ```
   Loading composer repositories with package information
   Installing dependencies (including require-dev) from lock file
   Package operations: 71 installs, 0 updates, 0 removals
     - Installing bower-asset/jquery (3.4.1): Downloading (100%)         
     - Installing bower-asset/inputmask (3.3.11): Downloading (100%)         
     - Installing bower-asset/punycode (v1.3.2): Downloading (100%)         
     - Installing bower-asset/yii2-pjax (2.0.7.1): Downloading (100%)         
     - Installing cebe/markdown (1.2.1): Downloading (100%)         
     - Installing doctrine/lexer (1.0.2): Downloading (100%)         
     - Installing egulias/email-validator (2.1.11): Downloading (100%)         
     - Installing ezyang/htmlpurifier (v4.11.0): Downloading (100%)         
     - Installing symfony/polyfill-iconv (v1.12.0): Downloading (100%)         
     - Installing symfony/polyfill-php72 (v1.12.0): Downloading (100%)         
     - Installing symfony/polyfill-mbstring (v1.12.0): Downloading (100%)         
     - Installing symfony/polyfill-intl-idn (v1.12.0): Downloading (100%)         
     - Installing yiisoft/yii2 (2.0.27): Downloading (100%)         
     - Installing bower-asset/bootstrap (v3.4.1): Downloading (100%)         
     - Installing yiisoft/yii2-bootstrap (2.0.10): Downloading (100%)         
     - Installing opis/closure (3.4.0): Downloading (100%)         
     - Installing yiisoft/yii2-debug (2.1.9): Downloading (100%)         
     - Installing phpspec/php-diff (v1.1.0): Downloading (100%)         
     - Installing yiisoft/yii2-gii (2.1.1): Downloading (100%)         
     - Installing swiftmailer/swiftmailer (v6.2.1): Downloading (100%)         
     - Installing yiisoft/yii2-swiftmailer (2.1.2): Downloading (100%)         
     - Installing symfony/polyfill-ctype (v1.12.0): Downloading (100%)         
     - Installing symfony/yaml (v4.3.4): Downloading (100%)         
     - Installing symfony/finder (v4.3.4): Downloading (100%)         
     - Installing symfony/event-dispatcher-contracts (v1.1.5): Downloading (100%)         
     - Installing symfony/event-dispatcher (v4.3.4): Downloading (100%)         
     - Installing symfony/dom-crawler (v4.3.4): Downloading (100%)         
     - Installing symfony/css-selector (v4.3.4): Downloading (100%)         
     - Installing psr/container (1.0.0): Downloading (100%)         
     - Installing symfony/service-contracts (v1.1.6): Downloading (100%)         
     - Installing symfony/polyfill-php73 (v1.12.0): Downloading (100%)         
     - Installing symfony/console (v4.3.4): Downloading (100%)         
     - Installing symfony/browser-kit (v4.2.4): Downloading (100%)         
     - Installing sebastian/diff (2.0.1): Downloading (100%)         
     - Installing sebastian/recursion-context (3.0.0): Downloading (100%)         
     - Installing sebastian/exporter (3.1.2): Downloading (100%)         
     - Installing sebastian/comparator (2.1.3): Downloading (100%)         
     - Installing sebastian/version (2.0.1): Downloading (100%)         
     - Installing sebastian/resource-operations (1.0.0): Downloading (100%)         
     - Installing sebastian/object-reflector (1.1.1): Downloading (100%)         
     - Installing sebastian/object-enumerator (3.0.3): Downloading (100%)         
     - Installing sebastian/global-state (2.0.0): Downloading (100%)         
     - Installing sebastian/environment (3.1.0): Downloading (100%)         
     - Installing phpunit/php-text-template (1.2.1): Downloading (100%)         
     - Installing doctrine/instantiator (1.2.0): Downloading (100%)         
     - Installing phpunit/phpunit-mock-objects (5.0.10): Downloading (100%)         
     - Installing phpunit/php-timer (1.0.9): Downloading (100%)         
     - Installing phpunit/php-file-iterator (1.4.5): Downloading (100%)         
     - Installing theseer/tokenizer (1.1.3): Downloading (100%)         
     - Installing sebastian/code-unit-reverse-lookup (1.0.1): Downloading (100%)         
     - Installing phpunit/php-token-stream (2.0.2): Downloading (100%)         
     - Installing phpunit/php-code-coverage (5.3.2): Downloading (100%)         
     - Installing webmozart/assert (1.5.0): Downloading (100%)         
     - Installing phpdocumentor/reflection-common (2.0.0): Downloading (100%)         
     - Installing phpdocumentor/type-resolver (1.0.1): Downloading (100%)         
     - Installing phpdocumentor/reflection-docblock (4.3.2): Downloading (100%)         
     - Installing phpspec/prophecy (1.8.1): Downloading (100%)         
     - Installing phar-io/version (1.0.1): Downloading (100%)         
     - Installing phar-io/manifest (1.0.1): Downloading (100%)         
     - Installing myclabs/deep-copy (1.9.3): Downloading (100%)         
     - Installing phpunit/phpunit (6.5.14): Downloading (100%)         
     - Installing ralouphie/getallheaders (3.0.3): Downloading (100%)         
     - Installing psr/http-message (1.0.1): Downloading (100%)         
     - Installing guzzlehttp/psr7 (1.6.1): Downloading (100%)         
     - Installing codeception/stub (1.0.4): Downloading (100%)         
     - Installing behat/gherkin (v4.4.5): Downloading (100%)         
     - Installing codeception/base (2.3.9): Downloading (100%)         
     - Installing codeception/specify (0.4.6): Downloading (100%)         
     - Installing codeception/verify (0.4.0): Downloading (100%)         
     - Installing fzaninotto/faker (v1.8.0): Downloading (100%)         
     - Installing yiisoft/yii2-faker (2.0.4): Downloading (100%)
   Generating autoload files
   > yii\composer\Installer::postInstall
   ```

5. Jalankan `docker-compose`

    ```
     docker-compose up -d
    ```

6. Browse ke `http://<ip-server>/index.php?r=bukutamu/index`. Akan muncul error karena tabel database belum dibuat.

    [![Error tampilkan data](img/01-error-database.png)](img/01-error-database.png)

7. Cari nama container mysql

    ```
     docker ps -a
    ```
   
   Outputnya seperti ini
   
   ```
   CONTAINER ID        IMAGE                       COMMAND                  CREATED             STATUS              PORTS                  NAMES
   0ba02d67c0b7        endymuhardin/yii-bukutamu   "docker-php-entrypoi…"   2 minutes ago       Up 3 seconds        0.0.0.0:8080->80/tcp   yiibukutamu_aplikasi_1
   74b63a53a9e1        mysql:5.7                   "docker-entrypoint.s…"   16 minutes ago      Up 4 seconds        3306/tcp, 33060/tcp    yiibukutamu_databaseserver_1
   ```

8. Login ke container database

    ```
    docker exec -it yiibukutamu_databaseserver_1 /bin/bash
    ```

9. Login ke mysql

    ```
    mysql -u bukutamu -p bukutamudb
    Enter password: 
    Reading table information for completion of table and column names
    You can turn off this feature to get a quicker startup with -A
    
    Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 2
    Server version: 5.7.27 MySQL Community Server (GPL)
    
    Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.
    
    Oracle is a registered trademark of Oracle Corporation and/or its
    affiliates. Other names may be trademarks of their respective
    owners.
    
    Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
    
    mysql>
    ```

10. Jalankan SQL untuk create table dan insert data

    ```sql
    create table bukutamu (
      id VARCHAR(36),
      email varchar (100) not null,
      komentar varchar (255) not null,
      waktu_mengisi timestamp not null,
      primary key (id)
    );

    insert into bukutamu (id, email, komentar, waktu_mengisi)
    values ('t001', 'tamu001@gmail.com', 'Ini komentar pertama', '2019-10-02 08:59:59');
    ```

11. Reload halaman web `http://<ip-server>/index.php?r=bukutamu/index`

    [![Sukses tampilkan data](img/02-sukses-database.png)](img/02-sukses-database.png)