<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Buku Tamu</h1>
<ul>
<?php foreach ($data as $d): ?>
    <li>
        <?= Html::encode("{$d->email}") ?>:
        <?= $d->komentar ?>
    </li>
<?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>